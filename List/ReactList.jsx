import React from "react";

class ReactList extends React.Component {
  constructor() {
    super();
    this.state = {
      student: [
        { name: "RAm", age: "20", gender: "male" },
        { name: "ganga", age: "21", gender: "female" },
        { name: "abbaz", age: "19", gender: "male" },
        { name: "RAj", age: "20", gender: "male" },
        { name: "ravi", age: "22", gender: "male" },
        { name: "mani", age: "20", gender: "male" },
        { name: "malar", age: "21", gender: "female" }
      ]
    };
  }
  render() {
    return (
      <div>
        {this.state.student.map((stud, i) => (
          <TableRow key={i} student={stud} />
        ))}
      </div>
    );
  }
}

class TableRow extends React.Component {
  render() {
    return (
      <tr>
        <td>Student Name:{this.props.student.name} /</td>
        <td>student Age:{this.props.student.age} /</td>
        <td>student Gender:{this.props.student.gender} </td>
      </tr>
    );
  }
}
export default ReactList;