import React from "react";
import ReactDOM from "react-dom";

import ReactList from "./ReactList";

const rootElement = document.getElementById("root");
ReactDOM.render(<ReactList />, rootElement);
