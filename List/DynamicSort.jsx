import React from "react";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      student: [
        { name: "babu", age: "22", gender: "male" },
        { name: "Ganga", age: "21", gender: "female" },
        { name: "Abbaz", age: "19", gender: "male" },
        { name: "rAj", age: "25", gender: "male" },
        { name: "Aggi", age: "23", gender: "male" },
        { name: "tom", age: "20", gender: "male" },
        { name: "Malar", age: "18", gender: "female" }
      ]
    };
  }

  sortBYAge = () => {
    let sortedAge;
    sortedAge = this.state.student.sort((a, b) => {
      return a.age - b.age;
    });

    this.setState({
      student: sortedAge
    });
  };

  sortBYName = () => {
    let sortedName;
    sortedName = this.state.student.sort(function(a, b) {
      var nameA = a.name.toLowerCase(); // ignore upper and lowercase
      var nameB = b.name.toLowerCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // names must be equal
      return 0;
    });
    this.setState({
      student: sortedName
    });
  };

  render() {
    return (
      <div>
        <div>Sort By Age</div>
        <button onClick={this.sortBYAge}>Age</button>
        <div> Sort By Name </div>
        <button onClick={this.sortBYName}>Name</button>
        {this.state.student.map((stud, i) => (
          <TableRow key={i} student={stud} />
        ))}
      </div>
    );
  }
}

class TableRow extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.student.name} /</td>
        <td>{this.props.student.age} /</td>
        <td>{this.props.student.gender} </td>
      </tr>
    );
  }
}

export default App;
