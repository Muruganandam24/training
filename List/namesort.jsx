import React from "react";
import ReactList from "./agesort";

class ReactList extends React.Component {
  constructor() {
    super();
    this.state = {
      student: [
        { name: "babu", age: "22", gender: "male" },
        { name: "Ganga", age: "21", gender: "female" },
        { name: "Abbaz", age: "19", gender: "male" },
        { name: "rAj", age: "25", gender: "male" },
        { name: "Aggi", age: "23", gender: "male" },
        { name: "tom", age: "20", gender: "male" },
        { name: "Malar", age: "18", gender: "female" }
      ]
    };
  }

  render() {
    this.state.student.sort(function(a, b) {
      var nameA = a.name.toLowerCase(); // ignore upper and lowercase
      var nameB = b.name.toLowerCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // names must be equal
      return 0;
    });
    return (
      <div>
        Sort List By Name
        {this.state.student.map((stud, i) => (
          <TableRow key={i} student={stud} />
        ))}
      </div>
    );
  }
}

class TableRow extends React.Component {
  render() {
    return (
      <tr>
        <td>Student Name:{this.props.student.name} /</td>
        <td>student Age:{this.props.student.age} /</td>
        <td>student Gender:{this.props.student.gender} </td>
      </tr>
    );
  }
}
export default ReactList;
