
//Looping array [index] using For Loop
var tamilnadu= [{location:"coimbatore" , population:"3743565"},
                {location:"pollachi" , population:"2343565"},
                {location:"chennai" , population:"6743565"},
                {location:"madurai" , population:"1743565"},
                {location:"dindukal" , population:"2743565"},
                {location:"erode" , population:"3743565"},
                {location:"karur" , population:"4743565"},
                {location:"palani" , population:"5743565"},
                {location:"vellore" , population:"7743565"},
                {location:"ooty" , population:"8743565"}
                ];

console.log("Districts and population List:");
             
for(let i=0;i < tamilnadu.length;i++){
    console.log(tamilnadu[i].location + tamilnadu[i].population);
}
console.log("Districts List:");

for(let i=0;i < tamilnadu.length;i++){
    console.log(tamilnadu[i].location);
}

//Looping Array Javascript Es6

var tamilnadu= [{location:"coimbatore" , population:"3743565"},
                {location:"pollachi" , population:"2343565"},
                {location:"chennai" , population:"6743565"},
                {location:"madurai" , population:"1743565"},
                {location:"dindukal" , population:"2743565"},
                {location:"erode" , population:"3743565"},
                {location:"karur" , population:"4743565"},
                {location:"palani" , population:"5743565"},
                {location:"vellore" , population:"7743565"},
                {location:"ooty" , population:"8743565"}
               ];

 Object.entries(tamilnadu).forEach(([key,values])=>
console.log(`${values.location}:${values.population}`)
); 
Object.entries(tamilnadu).forEach(([key,values])=>
console.log(`${values.location}`)
);

//Looping Array Using For Loop

var tamilnadu= [{location:"coimbatore" , population:"3743565"},
                {location:"pollachi" , population:"2343565"},
                {location:"chennai" , population:"6743565"},
                {location:"madurai" , population:"1743565"},
                {location:"dindukal" , population:"2743565"},
                {location:"erode" , population:"3743565"},
                {location:"karur" , population:"4743565"},
                {location:"palani" , population:"5743565"},
                {location:"vellore" , population:"7743565"},
                {location:"ooty" , population:"8743565"}
                ];
               
console.log("Districts and population");

for(let values in tamilnadu){
    console.log(`${tamilnadu[values].location} ${tamilnadu[values].population}`);
}

console.log("------------");

console.log("Districts in tamilnadu");

for(let values in tamilnadu){
    console.log(`${tamilnadu[values].location}`);
}
